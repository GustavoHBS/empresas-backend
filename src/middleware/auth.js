const jwt = require("jsonwebtoken");
const authConfig = require("../config/auth.json");
const NO_TOKEN_MESSAGE = "No token provided";
const INVALID_TOKEN_MESSAGE = "Token invalided";

function validateToken(req, res, next) {
	const { client, "access-token": accessToken, uid } = req.headers;
	if (!client || !accessToken || !uid) {
		return res.status(401).send({
			error: NO_TOKEN_MESSAGE
		});
	}

	jwt.verify(accessToken, authConfig.secret, function(err, decoded){
		if(err){
			return res.status(401).send({
				error: INVALID_TOKEN_MESSAGE
			});
		}
		req.userId = decoded.id;
		return next();
	});
}

module.exports = {
	validateToken
};