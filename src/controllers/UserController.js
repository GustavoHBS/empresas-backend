const User = require("../models/User");
const jwt = require("jsonwebtoken");
const authConfig = require("../config/auth.json")
const NOT_FOUND_MESSAGE = "User not found";
const TOKEN_EXPIRE_TIME = 1581860503;

async function auth(req, res) {
	const { email, password } = req.body;
	const user = await findUser({ email, password });
	if(validUser(user, res)){
		let { header, body } = generateHeaderAndBody(user, email);
		return res.set(header).json(body);
	}
}

async function findUser(where){
	return await User.findOne({
		where
	});
}

function validUser(user, res){
	let isValidUser = true;
	if (!user) {
		isValidUser = false;
		res.status(400).send({
			error: NOT_FOUND_MESSAGE
		});
	}
	return isValidUser;
}

function generateHeaderAndBody(user, email){
	const token = generateToken(email);
	delete user.dataValues.password;
	let header = {
		client: authConfig.secret,
		"access-token": token,
		uid: email,
		expiry: TOKEN_EXPIRE_TIME
	};
	return {
		header,
		body: user
	};
}

function generateToken(id){
	return jwt.sign({ id }, authConfig.secret, {
		expiresIn: TOKEN_EXPIRE_TIME
	});
}

module.exports = {
	auth
};