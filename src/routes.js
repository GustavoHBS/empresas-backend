const express = require('express');
const auth = require("./middleware/auth");
const UserController = require('./controllers/UserController');
const routes = express.Router();
const VERSION_API = getPackageVersion();
const API_PATH = `/api/v${VERSION_API}`;

routes.post(`${API_PATH}/users/auth/sign_in`, UserController.auth);
routes.use(auth);

function getPackageVersion(){
	let version = require('../package.json').version;
	return version.match(/^[0-9]{1,}/)[0];
}

module.exports = routes;